import requests

url = "https://helium-api.stakejoy.com/v1/hotspots/11aLRiiEQxtAKBz3pQzFb1MkUpizu67mBabaogptVV7C62xDzGd/rewards/sum?min_time=2021-09-15T12:00:00.000Z&max_time=2021-10-28T09:32:00.781Z&bucket=day"

headers = {
    'authority': 'helium-api.stakejoy.com',
    'pragma': 'no-cache',
    'cache-control': 'no-cache',
    'sec-ch-ua': '"Google Chrome";v="95", "Chromium";v="95", ";Not A Brand";v="99"',
    'accept': 'application/json, text/plain, */*',
    'sec-ch-ua-mobile': '?0',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.54 Safari/537.36',
    'sec-ch-ua-platform': '"Windows"',
    'origin': 'https://explorer.helium.com',
    'sec-fetch-site': 'cross-site',
    'sec-fetch-mode': 'cors',
    'sec-fetch-dest': 'empty',
    'referer': 'https://explorer.helium.com/',
    'accept-language': 'en-US,en;q=0.9'
}

response = requests.request("GET", url, headers=headers)

daily_profits = response.json()['data']
total = 0.0
for profit in daily_profits:
    print(profit['timestamp'] + " -> " + str(profit['total']))
    total += profit['total']
print(total/2)
